package agile.planer.domain

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "notes")
data class Note(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(nullable = false)
        var title: String = "",

        @Column(nullable = false)
        var done: Boolean = false,

        var dateTime: LocalDateTime = LocalDateTime.now()

)