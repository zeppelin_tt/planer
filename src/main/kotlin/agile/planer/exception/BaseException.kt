package agile.planer.exception

data class BaseException(val errorCode: ErrorCode, val errorMessage: String? = null)
    : RuntimeException(errorMessage ?: errorCode.defaultMessage)