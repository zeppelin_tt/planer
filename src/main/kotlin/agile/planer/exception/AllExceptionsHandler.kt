package agile.planer.exception

import agile.planer.dto.BaseError
import agile.planer.util.logger
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class AllExceptionsHandler : ResponseEntityExceptionHandler() {

    companion object {
        val L by logger()
    }

    @ExceptionHandler(BaseException::class)
    fun handleBaseException(ex: BaseException, request: WebRequest): ResponseEntity<Any> {
        L.debug("BaseException details: Error code: ${ex.errorCode}, text: ${ex.localizedMessage}")
        return handleExceptionInternal(ex, BaseError(ex.localizedMessage),
                HttpHeaders(), ex.errorCode.httpStatus, request)
    }

    @ExceptionHandler(Exception::class)
    fun customHandleException(ex: Exception, request: WebRequest): ResponseEntity<Any> {
        L.error("Exception details: ${ex.localizedMessage}", ex)
        return ResponseEntity(BaseError(ErrorCode.INTERNAL.defaultMessage), ErrorCode.INTERNAL.httpStatus)
    }
}