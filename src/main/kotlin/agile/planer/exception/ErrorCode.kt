package agile.planer.exception

import org.springframework.http.HttpStatus

enum class ErrorCode(val httpStatus: HttpStatus, val defaultMessage: String) {
    INTERNAL(HttpStatus.INTERNAL_SERVER_ERROR, "Произошла внутренняя ошибка при обработке запроса"),
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "Указаны некорректные данные, запрос не выполнен"),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "Необходимо пройти авторизацию"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "Доступ запрещён");
}