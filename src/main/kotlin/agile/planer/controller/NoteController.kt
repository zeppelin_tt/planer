package agile.planer.controller

import agile.planer.domain.Note
import agile.planer.service.NoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotNull

@RestController
@RequestMapping("notes")
class UserController @Autowired constructor(private val noteService: NoteService) {

    @GetMapping
    fun getAll() = noteService.getAll()

    @MessageMapping("/get_notes")
    @SendTo("/topic/activity")
    fun wsGetAll() = noteService.getAll()

    @MessageMapping("/create_note")
    @SendTo("/topic/command")
    fun wsCreate(@Payload @NotNull note: Note) = noteService.wsCreate(note)

    @MessageMapping("/update_note")
    @SendTo("/topic/command")
    fun wsUpdate(@Payload @NotNull note: Note) = noteService.wsUpdate(note)

    @MessageMapping("/toggle_note")
    @SendTo("/topic/command")
    fun wsToggleNote(@Payload @NotNull note: Note) = noteService.wsToggle(note)

    @MessageMapping("/delete_note")
    @SendTo("/topic/command")
    fun wsDelete(@NotNull id: String) = noteService.wsDelete(id.toLong())

    @MessageMapping("/delete_all")
    @SendTo("/topic/command")
    fun wsDeleteAll() = noteService.wsDeleteAll()

}