package agile.planer.dto

data class BaseError(val error: String)