package agile.planer.dto

import agile.planer.domain.Note


data class Command(
        val cmd: String = "",
        val id: Long? = null,
        val note: Note? = null,
        val unfinishedCount: Long? = null
)