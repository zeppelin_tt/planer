package agile.planer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PlanerApplication

fun main(args: Array<String>) {
	runApplication<PlanerApplication>(*args)
}
