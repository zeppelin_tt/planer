package agile.planer.service

import agile.planer.domain.Note
import agile.planer.dto.Command
import agile.planer.exception.BaseException
import agile.planer.exception.ErrorCode
import agile.planer.repository.NoteRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.time.LocalDateTime

private val log = KotlinLogging.logger {}

@Service
class NoteService(private val noteRepository: NoteRepository) {

    fun getAll(): List<Note> = noteRepository.getOrderedAll()

    fun save(note: Note): Note = noteRepository.save(note)

    fun wsDelete(id: Long): Command {
        checkNoteExistence(id)
        noteRepository.deleteById(id)
        return Command(cmd = "delete", id = id)
    }

    fun wsUpdate(note: Note): Command {
        checkNoteExistence(note.id)
        log.info { note.toString() }
        note.dateTime = LocalDateTime.now()
        val dbNote = save(note)
        return Command(cmd = "update", note = dbNote, unfinishedCount = noteRepository.countByDone(false))
    }

    fun wsToggle(note: Note): Command {
        note.done = !note.done
        return wsUpdate(note)
    }

    fun wsCreate(note: Note): Command {
        note.dateTime = LocalDateTime.now()
        val dbNote = save(note)
        return Command(cmd = "create", note = dbNote, unfinishedCount = noteRepository.countByDone(false))
    }

    fun wsDeleteAll(): Command {
        noteRepository.deleteAll()
        return Command(cmd = "delete_all")
    }

    private fun checkNoteExistence(id: Long) {
        if (!noteRepository.existsById(id)) {
            throw BaseException(ErrorCode.BAD_REQUEST, "Note с id: $id не существует")
        }
    }

}