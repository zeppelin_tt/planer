package agile.planer.repository

import agile.planer.domain.Note
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface NoteRepository : JpaRepository<Note, Long> {

    @Query(value = """(select * from notes where not is_done order by date_time)
                        union all
                      (select * from notes where is_done order by date_time desc)""",
            nativeQuery = true)
    fun getOrderedAll(): List<Note>

    fun countByDone(isDone: Boolean): Long

}
